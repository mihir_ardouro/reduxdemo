import React from "react";

import ScreenView from "../components/ScreenView";

class Screen1 extends React.Component {
  render() {
    return <ScreenView screenName="Screen 1" />;
  }
}

export default Screen1;
