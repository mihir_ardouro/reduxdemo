import React from "react";

import ScreenView from "../components/ScreenView";

class Screen4 extends React.Component {
  render() {
    return <ScreenView screenName="Screen 4" />;
  }
}

export default Screen4;
