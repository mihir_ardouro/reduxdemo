import React from "react";

import ScreenView from "../components/ScreenView";

class Screen3 extends React.Component {
  render() {
    return <ScreenView screenName="Screen 3" />;
  }
}

export default Screen3;
