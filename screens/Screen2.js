import React from "react";

import ScreenView from "../components/ScreenView";

class Screen2 extends React.Component {
  render() {
    return <ScreenView screenName="Screen 2" />;
  }
}

export default Screen2;
