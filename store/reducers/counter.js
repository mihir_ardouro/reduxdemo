const { INCREMENT_COUNTER, DECREMENT_COUNTER } = require("../actions/counter");

const initialState = {
  counterValue: 0,
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return { counterValue: state.counterValue + 1 };
    case DECREMENT_COUNTER:
      return { counterValue: state.counterValue - 1 };
    default:
      return state;
  }
};

export default counterReducer;
