export const INCREMENT_COUNTER = "INCREMENT_COUNTER";
export const DECREMENT_COUNTER = "DECREMENT_COUNTER";

export const incrementCounter = (counterValue) => {
  return {
    type: INCREMENT_COUNTER,
    counterValue: counterValue,
  };
};

export const decrementCounter = (counterValue) => {
  return {
    type: DECREMENT_COUNTER,
    counterValue: counterValue,
  };
};
