import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createAppContainer } from "react-navigation";
import { Ionicons } from "@expo/vector-icons";

import Screen1 from "./screens/Screen1";
import Screen2 from "./screens/Screen2";
import Screen3 from "./screens/Screen3";
import Screen4 from "./screens/Screen4";

const MainNavigator = createBottomTabNavigator(
  {
    Screen1: Screen1,
    Screen2: Screen2,
    Screen3: Screen3,
    Screen4: Screen4,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        return <Ionicons name="ios-star" size={25} color={tintColor} />;
      },
    }),
  }
);

export default createAppContainer(MainNavigator);
