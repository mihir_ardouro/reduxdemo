import React from "react";
import { enableScreens } from "react-native-screens";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";

import counterReducer from "./store/reducers/counter";

enableScreens();

const rootReducer = combineReducers({
  counter: counterReducer,
});

const store = createStore(rootReducer);

import MainNavigator from "./Navigator";

export default function App() {
  return (
    <Provider store={store}>
      <MainNavigator />
    </Provider>
  );
}
