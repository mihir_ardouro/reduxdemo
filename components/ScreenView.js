import React from "react";
import { connect } from "react-redux";
import { View, Text, Button, StyleSheet } from "react-native";

import { incrementCounter, decrementCounter } from "../store/actions/counter";

class ScreenView extends React.Component {
  incrementCounterHandler = (newCounterValue) => {
    this.props.incrementCounter(newCounterValue);
  };

  decrementCounterHandler = (newCounterValue) => {
    this.props.decrementCounter(newCounterValue);
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.props.screenName}</Text>
        <Text style={{ fontSize: 100 }}>{this.props.counterValue}</Text>
        <View style={{ flexDirection: "row" }}>
          <View style={{ padding: 10 }}>
            <Button
              title="Increment"
              onPress={() =>
                this.incrementCounterHandler(this.props.counterValue)
              }
            />
          </View>
          <View style={{ padding: 10 }}>
            <Button
              title="Decrement"
              onPress={() =>
                this.decrementCounterHandler(this.props.counterValue)
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => {
  return { counterValue: state.counter.counterValue };
};

const mapDispatchToProps = {
  incrementCounter,
  decrementCounter,
};

export default connect(mapStateToProps, mapDispatchToProps)(ScreenView);
